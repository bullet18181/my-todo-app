# Laravel ToDo App

## Installation

Clone the repository-
```
git clone git@gitlab.com:bullet18181/my-todo-app.git
```

Then cd into the folder with this command-
```
cd my-todo-app
```

Then do a composer install
```
composer install
```

Then create a environment file using this command-
```
cp .env.example .env
```

Then edit `.env` change it to your desire dbusername and dbpassword (`DB_USERNAME`, `DB_PASSWORD`).

Then create a database named `todoDB` and then do a database migration using this command-
```
php artisan migrate
```

Run key generate command for hashing
```
php artisan key:generate
```

```
php artisan serve
```

Then go to `http://localhost:8000` from your browser and see the app.